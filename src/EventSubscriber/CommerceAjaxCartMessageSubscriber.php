<?php

namespace Drupal\commerce_ajax_cart_message\EventSubscriber;

use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\EventSubscriber\CartEventSubscriber;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Commerce ajax cart message event subscriber.
 */
class CommerceAjaxCartMessageSubscriber extends CartEventSubscriber {

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  /**
   * {@inheritdoc}
   */
  public function displayAddToCartMessage(CartEntityAddEvent $event) {
    $is_ajax = $this->currentRequest->isXmlHttpRequest();
    if (!$is_ajax) {
      parent::displayAddToCartMessage($event);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setCurrentRequest(RequestStack $requestStack) {
    $this->currentRequest = $requestStack->getCurrentRequest();
  }

}
